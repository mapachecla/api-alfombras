const express = require('express');
const userController = require('../../controllers/v1/user.controller');
const md_auth = require('../../middlewares/auth');

const route = express.Router();

// crear usuario admin por unica vez
route.post('/register-admin', userController.saveAdmin);
route.post('/register-emp', md_auth.isAdmin, userController.saveEmployee);
route.post('/register-sup', md_auth.isAdmin, userController.saveSupervisor);
route.put('/update-emp/:id', md_auth.isAdmin, userController.updateEmployee);





module.exports = route;