const userRoutes = require('./user.routes');
const authRoutes = require('./auth.routes');

module.exports = app => {
    app.use('/api/v1/users', userRoutes, authRoutes);
}