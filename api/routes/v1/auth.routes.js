const express = require('express');
const authController = require('../../controllers/v1/auth.controller');


const route = express.Router();

route.post('/login', authController.login);


module.exports = route;