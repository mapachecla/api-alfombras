'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const createToken = (user) => {
    try {
        const payload = {
            sub: user._id,
            username: user.username,
            email: user.email,
            nombre: user.nombre,
            apellido: user.apellido,
            role: user.role,
            iat: moment().unix(),
            exp: moment().add(30, 'days').unix()
        }
        return jwt.encode(payload, process.env.JWT_SECRET);
    } catch (e) {
        console.log('ERROR GENERATING TOKEN', e);
    }
}

module.exports = { createToken };