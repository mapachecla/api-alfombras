'use strict'

const User = require('../../models/user.model');
const bcrypt = require('bcrypt');

// --- CREA USUARIO ADMIN POR UNICA VEZ, LUEGO COMENTAR ESTA FUNCION --- //
const saveAdmin = async(req, res) => {
    try {
        const { password, password2 } = req.body;

        if (password && password2) {

            if (password === password2) {

                if (password2.length > 8) {

                    User.findOne({ email: 'admin@admin' }).exec((err, user) => {
                        if (user) {
                            res.json({ error: 'Usuario ADMIN ya se encuentra registrado, contacte al soporte' })
                        } else {

                            function saveUserAdmin() {
                                bcrypt.genSalt(10, (err, salt) => {
                                    if (err) throw err;
                                    bcrypt.hash(password2, salt, async(err, hash) => {
                                        if (err) throw err;

                                        const password = hash;
                                        const role = 'ROLE_ADMIN';
                                        const email = 'admin@admin';
                                        const username = 'admin';
                                        const newUser = new User({ username, email, password, role });

                                        await newUser.save((err, userStored) => {
                                            if (err) {
                                                if (err.code && err.code === 11000) {
                                                    console.log(err);
                                                    res.status(400).send({ message: 'El usuario admin ya existe, solicite nueva password a soporte técnico.' });
                                                }
                                            } else {
                                                if (!userStored) {
                                                    res.status(404).send({ status: 'ERROR', message: 'User has not registered' });
                                                } else {
                                                    console.log(userStored);
                                                    res.status(200).send({ user: userStored });
                                                }
                                            }
                                        })
                                    })
                                })
                            }
                            saveUserAdmin();
                        }
                    })

                } else {
                    res.status(400).send({ message: 'PASS MUY CORTO' });
                }

            } else {
                res.status(400).send({ message: 'PASS NO SON IGUALES' });
            }

        } else {
            res.status(400).send({ message: 'DATA INCOMPLETA' });
        }

    } catch (e) {
        console.log('Error create user ADMIN', e);
        res.status(500).send({ message: e.message });
    }
}

// --- CREAR USUARIO EMPLEADO --- //
const saveEmployee = async(req, res) => {
    try {
        const { username, email, password, password2 } = req.body;

        if (username && email && password && password2) {

            if (password && password2) {

                if (password === password2) {

                    if (password2.length > 8) {

                        function saveEmployee() {
                            bcrypt.genSalt(10, (err, salt) => {
                                if (err) throw err;
                                bcrypt.hash(password2, salt, async(err, hash) => {
                                    if (err) throw err;

                                    const password = hash;
                                    const role = 'ROLE_EMPL';
                                    const newUser = new User({ username, email, password, role });

                                    await newUser.save((err, userStored) => {
                                        if (err) {
                                            if (err.code && err.code === 11000) {
                                                console.log(err);
                                                res.status(400).send({ message: 'DUPLICATE VALUES' });
                                            }
                                        } else {
                                            if (!userStored) {
                                                res.status(404).send({ status: 'ERROR', message: 'User has not registered' });
                                            } else {
                                                console.log(userStored);
                                                res.status(200).send({ user: userStored });
                                            }
                                        }
                                    })
                                })
                            })
                        }
                        saveEmployee();

                    } else {
                        res.status(400).send({ message: 'PASS MUY CORTO' });
                    }

                } else {
                    res.status(400).send({ message: 'PASS NO SON IGUALES' });
                }

            } else {
                res.status(400).send({ message: 'DATA INCOMPLETA' });
            }

        } else {
            res.status(400).send({ message: 'DATA INCOMPLETA' });
        }

    } catch (e) {
        console.log('Error create user EMPLOYEE', e);
        res.status(500).send({ message: e.message });
    }
}

// --- CREAR USUARIO SUPERVISOR --- //
const saveSupervisor = async(req, res) => {
    try {
        const { username, email, password, password2 } = req.body;

        if (username && email && password && password2) {

            if (password && password2) {

                if (password === password2) {

                    if (password2.length > 8) {

                        // Buscar si ese username ya existe

                        function saveSupervisor() {
                            bcrypt.genSalt(10, (err, salt) => {
                                if (err) throw err;
                                bcrypt.hash(password2, salt, async(err, hash) => {
                                    if (err) throw err;

                                    const password = hash;
                                    const role = 'ROLE_SUPERV';
                                    const newUser = new User({ username, email, password, role });

                                    await newUser.save((err, userStored) => {
                                        if (err) {
                                            if (err.code && err.code === 11000) {
                                                console.log(err);
                                                res.status(400).send({ message: 'DUPLICATE VALUES' });
                                            }
                                        } else {
                                            if (!userStored) {
                                                res.status(404).send({ status: 'ERROR', message: 'User supervisor has not registered' });
                                            } else {
                                                console.log(userStored);
                                                res.status(200).send({ user: userStored });
                                            }
                                        }
                                    })
                                })
                            })
                        }
                        saveSupervisor();

                    } else {
                        res.status(400).send({ message: 'PASS MUY CORTO' });
                    }

                } else {
                    res.status(400).send({ message: 'PASS NO SON IGUALES' });
                }

            } else {
                res.status(400).send({ message: 'DATA INCOMPLETA' });
            }

        } else {
            res.status(400).send({ message: 'DATA INCOMPLETA' });
        }

    } catch (e) {
        console.log('Error create user SUPERVISOR');
        res.status(500).send({ message: e.message });
    }
}

// --- ACTUALIZAR EMPLEADO --- //
const updateEmployee = async(req, res) => {
    try {
        const userId = req.params.id;
        const update = req.body;

        if (!userId) {
            return res.status(400).send({ error: 'NO SE ENCONTRÓ EL USER POR URL' })
        }

        if (update.password == '' && update.password2 == '') {

            return res.json({ message: 'PASSWORDS VACIOS' });

        } else {

            if (!update.password && !update.password2) {

                User.findByIdAndUpdate(userId, update, (err, userUpdate) => {
                    if (err) {
                        res.status(400).send({ error: 'ERROR', message: 'ERROR USER UPDATED' });
                    } else {
                        if (!userUpdate) {
                            res.status(404).send({ error: 'ERROR', message: 'NOT USER UPDATED' });
                        } else {
                            res.status(200).send({ user: userUpdate });
                        }
                    }
                })

            } else {

                if (update.password && update.password2) {
                    if (update.password === update.password2) {
                        const passw = update.password
                        if (passw.length > 8) {

                            bcrypt.genSalt(10, (err, salt) => {
                                if (err) throw err
                                bcrypt.hash(update.password, salt, (err, hash) => {
                                    if (err) throw err
                                    update.password = hash
                                    console.log(`La pass encriptada es: ${update.password}`)

                                    // if (userId != req.user.sub) {
                                    //     return res.status(500).send({ error: 'ERROR', message: 'INVALID TOKEN' })
                                    // }

                                    User.findByIdAndUpdate(userId, update, (err, userUpdate) => {
                                        if (err) {
                                            res.status(500).send({ error: 'ERROR', message: 'ERROR_USER_UPDATED' })
                                        } else {
                                            if (!userUpdate) {
                                                res.status(404).send({ error: 'ERROR', message: 'NOT_USER_UPDATED' })
                                            } else {
                                                res.status(200).send({ user: userUpdate })
                                            }
                                        }
                                    })

                                })
                            })

                        } else {
                            res.status(400).send({ message: 'PASS MUY CORTO' })
                        }

                    } else {
                        res.status(400).send({ message: 'PASS NO SON IGUALES' })
                    }

                } else {
                    res.status(400).send({ message: 'INCOMPLETE DATA' })
                }

            }
        }

    } catch (e) {
        res.status(500).send({ error: e, message: 'SERVER ERROR' });
    }
}


module.exports = {
    saveAdmin,
    saveEmployee,
    saveSupervisor,
    updateEmployee
}