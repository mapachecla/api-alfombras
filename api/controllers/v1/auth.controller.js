'use strict'

const bcrypt = require('bcrypt');
const User = require('../../models/user.model');
const jwt = require('../../service/jwt');


const login = async(req, res) => {
    try {
        const { username, email, password } = req.body;
        // const params = req.body;

        if (password && email || username) {

            const user = await User.findOne({ email });
            if (user) {
                const isOk = await bcrypt.compare(password, user.password);
                if (isOk) {
                    // if (params.gethash) {
                    console.log(user);
                    res.status(200).send({ token: jwt.createToken(user) });
                    // } else {
                    //     console.log(user);
                    //     res.status(200).send({ message: 'LOGIN SUCCESS' });
                    // }
                } else {
                    res.status(401).send({ message: 'INVALID PASSWORD' });
                }

            } else {
                const user = await User.findOne({ username });
                if (user) {
                    const isOk = await bcrypt.compare(password, user.password);
                    if (isOk) {
                        if (params.gethash) {
                            console.log(user);
                            res.status(200).send({ token: jwt.createToken(user) });
                        } else {
                            console.log(user);
                            res.status(200).send({ message: 'LOGIN SUCCESS' });
                        }
                    } else {
                        res.status(401).send({ message: 'INVALID PASSWORD' });
                    }
                } else {
                    res.status(401).send({ message: 'USER NOT FOUND' });
                }
            }

        } else {
            res.status(400).json({ message: 'DATA INCOMPLETA' });
        }

    } catch (e) {
        res.status(500).send({ error: 'ERROR', message: e.message });
    }
}

module.exports = { login }