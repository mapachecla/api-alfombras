const jwtoken = require('jsonwebtoken');
const jwt = require('jwt-simple');
const moment = require('moment');

// --- Comprueba si el token es correcto --- //
const ensureAuth = (req, res, next) => {
    try {
        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autorizacion' });
        }

        const token = req.headers.authorization.replace(/['"']+/g, '');

        try {
            var payload = jwt.decode(token, process.env.JWT_SECRET);
            if (payload.exp <= moment().unix()) {
                return res.status(401).send({ status: 'ERROR', message: 'EXPIRED TOKEN' });
            }

        } catch (e) {
            return res.status(404).send({ status: 'ERROR', message: 'INVALID TOKEN' });
        }

    } catch (e) {
        console.log('Error middleware ensureAuth', e);
    }
};

// --- Validar solo por SUPERVISOR --- //
const isSuperv = (req, res, next) => {
    try {

        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' });
        }

        const token = req.headers.authorization.replace(/['"']+/g, '');

        const payload = jwtoken.decode(token, process.env.JWT_SECRET);

        if (payload.role !== 'ROLE_SUPERV') {
            throw {
                code: 403,
                status: 'ACCESS DENIED',
                message: 'Invalid token'
            }
        } else {
            next();
        }

    } catch (e) {
        res.status(e.code || 500).send({ status: e.status || 'ERROR', message: e.message });
    }
}

// --- Validar solo por ADMINISTRADOR --- //
const isAdmin = (req, res, next) => {
    try {

        if (!req.headers.authorization) {
            return res.status(403).send({ status: 'ERROR', message: 'La peticion no tiene la cabecera de autenticación' });
        }

        const token = req.headers.authorization.replace(/['"']+/g, '');

        const payload = jwtoken.decode(token, process.env.JWT_SECRET);

        if (payload.role !== 'ROLE_ADMIN') {
            throw {
                code: 403,
                status: 'ACCESS DENIED',
                message: 'Invalid token'
            }
        } else {
            next();
        }

    } catch (e) {
        res.status(e.code || 500).send({ status: e.status || 'ERROR', message: e.message });
    }
}



module.exports = {
    ensureAuth,
    isSuperv,
    isAdmin
}