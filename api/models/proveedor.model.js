'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const ProveedorSchema = new Schema({
    nombre_empresa: { type: String, max: 64 },
    nombre_representante: { type: String, max: 64 },
    apellido: { type: String, max: 64 },
    nombre: { type: String, max: 64 },
    telefono1: { type: Number, max: 99999999999999999999, require: true },
    telefono2: { type: Number, max: 99999999999999999999 },
    email: { type: String, trim: true, lowercase: true },
    direccion: { type: String, max: 64 },
    localidad: { type: String, max: 64 },
    provincia: { type: String, max: 64 },
    pais: { type: String, max: 64 },
    notas: { type: String, max: 120 },
    atencion: { type: String, max: 64 },
    producto: { type: Schema.ObjectId, ref: 'Producto' },
    productos_otros: { type: Array, default: [] },
    image: { type: String },
}, { timestamps: true });

module.exports = mongoose.model('Proveedor', ProveedorSchema);