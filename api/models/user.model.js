'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
    username: { type: String, trim: true, require: true, unique: true, max: 64 },
    password: { type: String, require: true },
    apellido: { type: String, max: 64 },
    nombre: { type: String, max: 64 },
    email: { type: String, trim: true, require: true, unique: true, lowercase: true },
    telefono1: { type: Number, max: 99999999999999999999 },
    telefono2: { type: Number, max: 99999999999999999999 },
    direccion: { type: String, max: 64 },
    localidad: { type: String, max: 64 },
    provincia: { type: String, max: 64 },
    pais: { type: String, max: 64 },
    image: { type: String },
    role: { type: String, enum: ['ROLE_ADMIN', 'ROLE_SUPERV', 'ROLE_EMPL'], default: 'ROLE_EMPL' },
}, { timestamps: true });

module.exports = mongoose.model('User', UserSchema);