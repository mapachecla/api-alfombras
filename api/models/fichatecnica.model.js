'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const FichaSchema = new Schema({
    nombre: { type: String, max: 64, require: true },
    cliente: { type: Schema.ObjectId, ref: 'Cliente', require: true },
    descripcion: { type: String, max: 80 },
    medida_orilla: { type: Number, max: 99999999999999999999, require: true },
    medida_fleco: { type: Number, max: 99999999999999999999, require: true },
    image1: { type: String },
    image2: { type: String },
    presupuestos_finalizados: { type: Array, default: [] },
    file_DOC: { type: String },
}, { timestamps: true });

module.exports = mongoose.model('Ficha', FichaSchema);