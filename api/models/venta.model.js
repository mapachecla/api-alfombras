'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const VentaSchema = new Schema({
    producto: { type: Schema.ObjectId, ref: 'Producto', require: true },
    producto_lista: { type: Array, default: [] },
    cantidad: { type: Number, require: true },
    cantidad_total_prod: { type: Number },
    total_precio: { type: Number },
    vendedor: { type: Schema.ObjectId, ref: 'User', require: true },
    comprador: { type: Schema.ObjectId, ref: 'Cliente' },
    tipo_pago: { type: String, max: 20 }
}, { timestamps: true });

module.exports = mongoose.model('Venta', VentaSchema);