'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const ServicioSchema = new Schema({
    nombre: { type: String, max: 64, require: true },
    tipo: { type: String, max: 64, require: true },
    operacion_COF: { type: String, max: 64, require: true },
    item_restauracion: { type: String, max: 64, require: true },
    precio: { type: Number, max: 99999999999999999999, require: true },
    unidad_medida: { type: String, max: 8, require: true },
    medida: { type: Number, max: 99999999999999999999, require: true }
}, { timestamps: true });

module.exports = mongoose.model('Servicio', ServicioSchema);