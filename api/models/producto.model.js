'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const ProductoSchema = new Schema({
    nombre: { type: String, max: 64, require: true },
    tipo: { type: String, max: 64, },
    cantidad: { type: Number, max: 99999999999999999999, require: true },
    precio_costo: { type: Number, max: 99999999999999999999 },
    precio: { type: Number, max: 99999999999999999999, require: true },
    elaboracion_fecha: { type: Number, max: 99999999999999999999 },
    vencimiento_fecha: { type: Number, max: 99999999999999999999 },
    lote: { type: String, max: 80 },
    color: { type: String, max: 20 },
    unidad_medida: { type: String, max: 10, require: true },
    valor_medida: { type: Number, max: 99999999999999999999, require: true },
    image: { type: String },
    proveedor: { type: Schema.ObjectId, ref: 'Proveedor', require: true }
}, { timestamps: true });

module.exports = mongoose.model('Producto', ProductoSchema);