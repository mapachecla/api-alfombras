'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const ClienteSchema = new Schema({
    apellido: { type: String, max: 64, require: true },
    nombre: { type: String, max: 64, require: true },
    email: { type: String, trim: true, lowercase: true },
    telefono1: { type: Number, max: 99999999999999999999, require: true },
    telefono2: { type: Number, max: 99999999999999999999 },
    empresa: { type: String, max: 64 },
    direccion: { type: String, max: 64 },
    localidad: { type: String, max: 64 },
    provincia: { type: String, max: 64 },
    pais: { type: String, max: 64 },
    notas: { type: String, max: 120 },
    image: { type: String },
}, { timestamps: true });

module.exports = mongoose.model('Cliente', ClienteSchema);