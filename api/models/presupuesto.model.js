'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const PresupuestoSchema = new Schema({
    cliente: { type: Schema.ObjectId, ref: 'Cliente', require: true },
    detalle: { type: Schema.ObjectId, ref: 'Servicio', require: true },
    ficha_tec: { type: Schema.ObjectId, ref: 'Ficha', require: true },
    cantidad_total: { type: Number, require: true },
    precio_total: { type: Number, require: true },
    observacion: { type: String, max: 80 },
    empleado: { type: Schema.ObjectId, ref: 'User', require: true },
    file_PDF: { type: String },
}, { timestamps: true });

module.exports = mongoose.model('Presupuesto', PresupuestoSchema);